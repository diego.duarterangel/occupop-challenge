import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import { Link } from "react-router-dom";

import { Form, Field } from "react-final-form";

interface FormValues {
  email?: string;
  password?: string;
}

const Login = () => {
  const validate = (values: FormValues) => {
    const errors: FormValues = {};
    if (!values.email) {
      errors.email = "Required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }
    if (!values.password) {
      errors.password = "Required";
    }
    return errors;
  };

  const onSubmit = (values: FormValues) => {
    console.log(values);
  };

  return (
    <div className="w-full h-screen">
      <div className="grid grid-cols-3 shadow-2xl shadow-gray-500">
        <div className="h-screen w-full col-span-1 bg-[#1AEBB6] flex justify-end items-center ">
          <div className="w-1/2 h-[600px] shadow-login-left">
            <div className="flex flex-col w-full items-center justify-center h-full gap-6">
              <div>
                <img
                  className="bg-transparent w-28"
                  src="/assets/logos/v.png"
                  alt=""
                />
                <div className="font-bold italic text-lg">CleanMyCar</div>
              </div>
              <div>
                <div>India's first waterless</div>
                <div>car cleaning company</div>
              </div>
            </div>
          </div>
        </div>
        <div className="h-screen w-full col-span-2">
          <div className="flex w-full justify-start items-center h-full">
            <div className="w-1/2 h-[600px] shadow-login-right">
              <div className="relative flex flex-col w-full items-center justify-center h-full">
                <div className="absolute top-4 right-10 cursor-pointer active:scale-95 transition-all duration-200 ease-in-out">
                  Need help?
                </div>
                <div className="text-lg font-bold">Log in</div>
                <Form
                  onSubmit={onSubmit}
                  validate={validate}
                  render={({ handleSubmit }) => (
                    <form
                      onSubmit={handleSubmit}
                      className="w-2/4 flex flex-col gap-2 mt-10"
                    >
                      <Field name="email">
                        {({ input, meta }) => (
                          <div>
                            <Input
                              {...input}
                              label="Email"
                              type="email"
                              placeholder="joe@email.com"
                            />
                            {meta.error && meta.touched && (
                              <span>{meta.error}</span>
                            )}
                          </div>
                        )}
                      </Field>
                      <Field name="password">
                        {({ input, meta }) => (
                          <div>
                            <Input
                              {...input}
                              label="Password"
                              type="password"
                              placeholder="Enter your Password"
                            />
                            {meta.error && meta.touched && (
                              <span>{meta.error}</span>
                            )}
                          </div>
                        )}
                      </Field>
                      <div className="text-xs text-right cursor-pointer">
                        <Link to="/forgot">Forgot password?</Link>
                      </div>
                      <div className="mt-10">
                        <Button>Login</Button>
                      </div>
                    </form>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
