type ButtonProps = {
  children: string;
  onClick?: () => void;
};

const Button: React.FC<ButtonProps> = ({ children }: ButtonProps) => {
  return (
    <button className="w-full bg-[#1AEBB6] text-black py-4 rounded-md transition-all duration-200 ease-in-out active:scale-95">
      {children}
    </button>
  );
};

export default Button;
