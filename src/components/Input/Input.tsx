type InputProps = {
  label: string;
  placeholder: string;
  type: "email" | "password" | "text" | "number";
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const Input: React.FC<InputProps> = ({
  label,
  placeholder,
  ...props
}: InputProps) => {
  return (
    <div className="flex flex-col gap-4">
      <label className="font-medium">{label}</label>
      <input
        className="border-2 outline-none px-4 py-2"
        placeholder={placeholder}
        {...props}
      />
    </div>
  );
};

export default Input;
