/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        "login-left":
          "0 -10px 0 0 rgba(0, 0, 0, 0.1), 0 10px 0 0 rgba(0, 0, 0, 0.1), -10px 0 0 0 rgba(0, 0, 0, 0.1)",
        "login-right":
          "0 -10px 10px 0 rgba(0, 0, 0, 0.1), 0 10px 10px 0 rgba(0, 0, 0, 0.1), 10px 0 10px 0 rgba(0, 0, 0, 0.1)",
      },
    },
  },
  plugins: [],
};
